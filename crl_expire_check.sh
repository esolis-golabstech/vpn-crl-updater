#!/usr/bin/env bash

set -e

CRL_FILE=$1

if [[ "${CRL_FILE}" == "" ]] ; then
    echo "Usage:"
    echo "    ${0} <CRL file path>"
    echo
    echo "Example:"
    echo "    ${0} pki/crl.pem
"
    exit 1
fi


expiration_date=$(openssl crl -in ${CRL_FILE} -noout -nextupdate | awk -F= '{print $2}')

current_date="$(date '+%s')"
expiration_date_epoch="$(date -d "${expiration_date}" '+%s')"
diff_days=$(( ("${expiration_date_epoch}" - "${current_date}") / (3600 * 24) ))

if [ "${diff_days}" -le "7" ]; then
	echo "Generate CRL. ${diff_days} days remaining."
else
	echo "${diff_days} days remaining."
fi